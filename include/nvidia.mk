.PHONY: nvidia-cuda

# Upload cudnn package here
nvidia-cudnn: 
	sudo apt install ${HOME}/Data/data-sync/packages/cudnn-local-repo-ubuntu2004-8.4.1.50_1.0-1_amd64.deb

nvidia-driver:
	sudo apt install nvidia-driver-515 && sudo dpkg-reconfigure nvidia-dkms-515

nvidia-cuda:
	wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin && sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
	wget https://developer.download.nvidia.com/compute/cuda/11.7.0/local_installers/cuda-repo-ubuntu2004-11-7-local_11.7.0-515.43.04-1_amd64.deb && sudo dpkg -i cuda-repo-ubuntu2004-11-7-local_11.7.0-515.43.04-1_amd64.deb
	sudo cp /var/cuda-repo-ubuntu2004-11-7-local/cuda*keyring.gpg /usr/share/keyrings/
	sudo apt update
	sudo apt install cuda
	echo "export PATH=${PATH}:/usr/local/cuda/bin && export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}/usr/local/cuda/lib" >> ${HOME}/.bashrc