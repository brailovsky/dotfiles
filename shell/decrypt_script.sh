DOT_SECRET_PASS=${1}
inp="${2}"  
out="${3}" 

cp "${inp}" /tmp/temp && \
echo ${DOT_SECRET_PASS} | gpg -q --batch --yes --for-your-eyes-only --passphrase-fd 0 --no-tty -d /tmp/temp > /tmp/temp.tar && \
mkdir -p "${out}" && tar -xf /tmp/temp.tar -C "${out}"
