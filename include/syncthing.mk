syncthing-configure:
	mkdir -p ${HOME}/.config/systemd/user/ && \
		cp /lib/systemd/system/syncthing@.service ${HOME}/.config/systemd/user/syncthing@${USER}.service && \
		systemctl enable syncthing@${USER} && \
		systemctl start syncthing@${USER} && \
	echo "WARNING: NEEDED A SYNCTHING CONFIGURATION INSTALLATION PART" && exit 1

syncthing-secret-dotfiles: syncthing-conf# .config/sublime-text-3, .ssh, .config/rclone 
	cp -R ${HOME}/Data/storage/dotfiles/* ${HOME}/