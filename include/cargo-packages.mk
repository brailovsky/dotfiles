cargo: install-deb-min
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --default-toolchain stable --profile minimal -y

git-absorb:  cargo
	cargo install git-absorb