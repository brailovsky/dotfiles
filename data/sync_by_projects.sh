# backup and sync data with rclone: 
# backup-sync my-mail-free ./data
backup-sync() {
	serv_name=$1
	local_folder_name=$2
	transfers=4 #12
	fold_name=backup_$(date +%F)
	ready_path=synced_shouldbe_removed
	rclone mkdir ${serv_name}:/${fold_name}/$(basename $(pwd))
	echo "rclone sync --progress ./${local_folder_name} ${serv_name}:/${fold_name}/$(basename $(pwd))"
	rclone sync --transfers=${transfers} --progress ./${local_folder_name} ${serv_name}:/${fold_name}/$(basename $(pwd)) && \
	mkdir -p ${ready_path}/${local_folder_name} && \
	mv ./${local_folder_name}/* ${ready_path}/${local_folder_name}
}
