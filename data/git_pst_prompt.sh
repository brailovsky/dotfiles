
# Git PS1
# See explanation of nmagic symbols here: 
# https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh#L33-L106

__git_ps1_venv() {
   # Virtualenv-compatible __git_ps1 function
   local pre="$1"
   local post="$2"

   if [ -n "${VIRTUAL_ENV}" ] && [ -z "${VIRTUAL_ENV_DISABLE_PROMPT:-}" ]; then
      if [ "`basename \"$VIRTUAL_ENV\"`" = "__" ] ; then
         pre="[`basename \`dirname \"$VIRTUAL_ENV\"\``] ${pre}"
      else
         pre="(`basename \"$VIRTUAL_ENV\"`) ${pre}"
      fi
   fi

   __git_ps1 "${pre}" "${post}"
}

# User specific aliases and functions
HOST_COLOR="\[\e[38;5;153m\]"
if [[ $EUID -ne 0 ]]; then
    USER_COLOR="\[\e[38;5;172m\]"
else
    USER_COLOR="\[\e[0;31m\]"
fi

PS1="[$USER_COLOR\u\[\e[m\]@$HOST_COLOR\h\[\e[m\] \W]\\$ "

unset USER_COLOR HOST_COLOR

if [ -f /usr/lib/git-core/git-sh-prompt ]; then
    source /usr/lib/git-core/git-sh-prompt

    GIT_PS1_SHOWCOLORHINTS=true
    GIT_PS1_SHOWDIRTYSTATE=true
    GIT_PS1_SHOWUNTRACKEDFILES=true
    GIT_PS1_SHOWUPSTREAM="auto"

    PROMPT_COMMAND="$(sed -r 's|^(.+)(\\\$\s*)$|__git_ps1_venv \"\1\" \"\\\2\"|' <<< $PS1)"

fi

