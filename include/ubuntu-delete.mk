######################### Unused consider to delete #########################
enable-alt-shift:
	dconf write /org/gnome/desktop/input-sources/xkb-options "['grp:alt_shift_toggle']"

docker-compose: install-deb-min
	curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o ~/.local/bin/docker-compose && \ 
	chmod u+x ~/.local/bin/docker-compose
	
mendeley:
 echo ">> DL tools..."  && \
 MEND_LOC=/tmp/mendeleydesktop-latest.deb && \
 curl -L https://www.mendeley.com/repositories/ubuntu/stable/amd64/mendeleydesktop-latest --output ${MEND_LOC} && \
 sudo apt install -y ${MEND_LOC} 

discord:
 DISCORD_LOC=/tmp/discord-0.0.13.deb && \
 curl -L https://dl.discordapp.net/apps/linux/0.0.13/discord-0.0.13.deb --output ${DISCORD_LOC} && \
 sudo apt install -y ${DISCORD_LOC}  
 
notion-old:
  wget https://notion.davidbailey.codes/notion-linux.list &&\
  sudo mv notion-linux.list /etc/apt/sources.list.d/notion-linux.list &&\
  sudo apt update && sudo apt install -y notion-desktop 
  
upwork:
	Upwork=/tmp/upwork.deb &&\
	curl -L https://upwork-usw2-desktopapp.upwork.com/binaries/v5_5_0_11_61df9c99b6df4e7b/upwork_5.5.0.11_amd64.deb --output ${Upwork} &&\
	sudo apt install -y ${Upwork}
