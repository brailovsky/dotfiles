
include include/nvidia.mk
include include/ubuntu.mk
include include/syncthing.mk
.PHONY: dotfiles-secrets install-vpns

create-base-folders:
	mkdir -p ${HOME}/.virtualenvs && mkdir -p ${HOME}/Development && \
	mkdir -p ${HOME}/.local/bin && mkdir -p ${HOME}/.local/dist

# Ubuntu configuration
install-deb-min: create-base-folders
	echo "Install minimal apt packages" && \
	sudo apt-get update && \
	sudo apt-get install -y openssh-client \
		htop curl tmux rsync rclone wget curl \
		git git-lfs python3 python3-pip ack grep vim \
		fuse libfuse2 apt-transport-https file
	sudo pip3 install virtualenv

_dvc:
	pip3 install --user dvc dvc[gdrive]

dotfiles-secrets-update:
	bash -c "source shell/init_secrets.sh && ./dotdrop.sh update -c ./config-secret.yaml; dvc add dotfiles_secret && dvc push && git commit -m\"feat: Update secrets\""

dotfiles-secrets-install: install-deb-min _dvc
	if test -f "./shell/init_secrets.sh"; then \
	bash -c "source ./shell/init_secrets.sh && dvc pull && dotdrop_install_secrets;" \
	else echo "Place secrets please"; fi

# dotfiles
dotfiles-all: install-deb-min
	pip3 install --user -r dotdrop/requirements.txt && \
	./dotdrop/bootstrap.sh && \
	./dotdrop.sh install --profile ubuntu-default && \
	cat data/git_pst_prompt.sh >> ${HOME}/.bashrc

install-ubuntu-max: install-deb-min git-absorb i-docker i-gui-apps-apt i-gui-apps-snap i-syncthing syncthing-configure i-k8s i-slicer i-yc chrome install-gotop

deb-update-kernel:
	export KERNEL_VERSION=5.13.0-35-generic
	sudo apt install linux-image-${KERNEL_VERSION} linux-headers-${KERNEL_VERSION} linux-modules-${KERNEL_VERSION} linux-modules-extra-${KERNEL_VERSION}
	# Test
	dpkg -l linux-headers-*${KERNEL_VERSION}* | grep ii
	dpkg -l linux-image-*${KERNEL_VERSION}* | grep ii

install-gotop:
	cd /tmp && \
	wget https://github.com/xxxserxxx/gotop/releases/download/v4.1.4/gotop_v4.1.4_linux_amd64.deb && \
	sudo apt install ./gotop_v4.1.4_linux_amd64.deb

_wg_config_generator_requirements:
	pip install -r ./third_parties/vpn_config/requirements.txt

install-vpns: _wg_config_generator_requirements
	bash shell/install_vpns.sh

install-yq:
	curl -L -f -o ~/.local/bin/yq https://github.com/mikefarah/yq/releases/download/v4.18.1/yq_linux_amd64 && chmod +x ~/.local/bin/yq