include include/cargo-packages.mk

apt-repo-add-sublime-merge: install-deb-min
	wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add - && \
	echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list

apt-repo-add-docker: install-deb-min
	echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
		$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# TODO: Install Freon and cpupower
# calibre sublime-text zeal
i-gui-apps-apt: apt-repo-add-sublime-merge
	sudo apt update && \
	sudo apt-get install -y lm-sensors gnome-tweaks \
		tilix vlc sublime-merge && \
		baobab && \
	sudo update-alternatives --set x-terminal-emulator /usr/bin/tilix.wrapper

#TODO: telegram-desktop todoist spotify pycharm-community chromium
i-gui-apps-snap:
	sudo snap install
	 	netron \
		gimp inkscape \
		zoom-client slack skype \
		simplescreenrecorder \
		code notion-snap \
		weasis \
	 	--no-wait --classic;

i-slicer:
	curl -L https://download.slicer.org/bitstream/62cc52d2aa08d161a31c1af0 --output /tmp/slicer.tgz && \
	mkdir -p ${HOME}/.local/dist/slicer && tar xf /tmp/slicer.tgz -C ${HOME}/.local/dist/slicer

i-docker: apt-repo-add-docker
	echo "Install docker..." && \
	sudo apt-get remove -y docker docker-engine docker.io containerd runc && \
	sudo apt update && \
	sudo apt-get install -y ca-certificates gnupg lsb-release && \
	sudo mkdir -p /etc/apt/keyrings && \
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
	echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
	$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null && \
	sudo apt update && \
	sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin && \
	sudo usermod -aG docker $USER && \
	sudo chmod 666 /var/run/docker.soc

i-syncthing:
	curl -s https://syncthing.net/release-key.txt | sudo apt-key add - && \
	echo "deb https://apt.syncthing.net/ syncthing release" | sudo tee /etc/apt/sources.list.d/syncthing.list && \
	sudo apt-get update && \
	sudo apt-get install -y syncthing

i-k8s:
	cd /tmp && \
	curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
	sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl && rm kubectl && \
	curl -L https://github.com/derailed/k9s/releases/download/v0.26.0/k9s_Linux_x86_64.tar.gz | tar zxv && mv k9s ${HOME}/.local/bin && \
	chmod u+x ${HOME}/.local/bin/k9s && \
	cd -

i-yc:
	curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
chrome:
	wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb --output-document /tmp/google-chrome-stable_current_amd64.deb && \
	sudo apt install /tmp/google-chrome-stable_current_amd64.deb && \
	sudo apt install google-chrome-stable

# export add_user=user
add-admin-user:
	apt-get install sudo && adduser ${add_user} && usermod -aG sudo ${add_user} && su ${add_user}