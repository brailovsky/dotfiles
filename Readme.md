# Dot files

Dotfiles and base system configuration.

## Usage

Full ubuntu configuration deploy

```bash
apt update && apt install -y make git && \
git clone --recursive https://gitlab.com/brailovsky/dotfiles.git ${HOME}/.dotfiles && \
cd ${HOME}/.dotfiles && make dotfiles-all install-ubuntu-max 
```

### Install secrets

Place `./shell/init_secrets.sh` file and run

```bash
make dotfiles-secrets-install
```

### Update secrets

```bash
make dotfiles-secrets-update
```

### Staging dot files pipeline

```bash
source shell/aliases 

# regular file
./dotdrop.sh import <path>

# secret file
dotdrop_import_secret_file <path>

# secret folder
dotdrop_import_secret_folder <path>

git add . && git commit -m"feat: Add new dotfiles"
```
