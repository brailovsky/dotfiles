for vpn_option in $(cat ${HOME}/.config/vpn/enable.list); do
    cd ./third_parties/vpn_config
    echo "Put config for ${vpn_option} to /tmp/dotfiles"
    python3 wg-config-generator.py ${vpn_option}
    cd - 
done

for wg_vpn_interface in  /tmp/dotfiles/*
do
    nmcli connection import type wireguard file ${wg_vpn_interface}
    nmcli connection modify ${wg_vpn_interface::-5} connection.autoconnect no
done

# Show connections
nmcli conn
